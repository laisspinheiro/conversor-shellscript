#!/bin/bash
moeda_base=$1
simbolo=$2
valor=$3 
cotacao=$(curl -s "https://api.exchangeratesapi.io/latest?base=$moeda_base&symbols=$simbolo" | jq ".rates.$simbolo")

total=$(python -c "print $valor * $cotacao")
echo "$total $simbolo" 